#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>
using namespace std;
string name1, name2;
int p1Score = 0, p2Score = 0;
int money1 = 0, money2 = 0;
int r1 = 0, r2 = 0;
int output = 0;

void drawBoard(string n1, string n2, int p1Level, int p2Level, int m1, int m2, int r1, int r2){
	cout << " ****Snake and Ladder Board***            *Where the Snake and Ladder Deployed*" <<endl;
	cout << "|--|--|--|--|--|--|--|--|--|--" << "           Ladder: Level 10 Goes to Level 13" << "           Jeepney: Level 20 Goes to Level 50" << endl;
	cout << "|41|42|43|44|44|46|47|48|49|50" << "           Ladder: Level 19 Goes to Level 25" << "           Jeepney: Level 15 Goes to Level 30" << endl;
	cout << "|--|--|--|--|--|--|--|--|--|--" << "           Ladder: Level 23 Goes to Level 35" << "           Rat: Level 27 " << endl;
	cout << "|40|39|38|37|36|35|34|33|32|31" << "           Ladder: Level 29 Goes to Level 44" << "           Rat: Level 38 " << endl;
	cout << "|--|--|--|--|--|--|--|--|--|--" << "           Ladder: Level 33 Goes to Level 50" << "           Money: Level 12  = 5" << endl;
	cout << "|21|22|23|24|25|26|27|28|29|30" << "           Snake: Level 49 Down to Level 2" <<   "             Money: Level 16  = 5" << endl;
	cout << "|--|--|--|--|--|--|--|--|--|--" << "           Snake: Level 25 Down to Level 10" << endl;
	cout << "|20|19|18|17|16|15|14|13|12|11" << "           Snake: Level 34 Down to Level 23" << endl;
	cout << "|--|--|--|--|--|--|--|--|--|--" << "           " << "**** Players Level Status ***" << endl;
	cout << "|1 |2 |3 |4 |5 |6 |7 |8 |9 |10" << "           " << n1 << " Current Level: " << p1Level <<  " Current Rat: " << r1 << endl;
	cout << "|--|--|--|--|--|--|--|--|--|--" << "           " << n1 << " Money: " << m1 << endl << endl;
	cout << "|SN|AK|_A|ND|_L|AD|DE|R!|!!|!!" << "           " << n2 << " Current Level: " << p2Level << " Current Rat: " << r2 << endl;
	cout << "|SN|AK|_A|ND|_L|AD|DE|R!|!!|!!" << "           " << n2 << " Money: " << m2 << endl << endl;
}


int dice(string name) { //random 1-6 stand as Dice
	cout << " " << name << " Rolling the dice" <<endl;
	srand(time(NULL));
	int n = rand() % 6 + 1;; // ramdom numbers, to pick the word.
	srand(n);

	return n;
}

int turn(int t) { //method to roll the dice of each player
	int output;
	if (t == 0) {
		output = dice(name1);
	}
	else {
		output = dice(name2);
	}
	return output;
}
int check1(int level) { // method to check for snake and ladders
	if (level == 10) {
		cout << "Lucky you go up" << endl;
		return 13;
	}else if(level == 19) {
		cout << "Lucky you go up" << endl;
		return 25;
	}
	else if (level == 23) {
		cout << "Lucky you go up" << endl;
		return 35;
	}
	else if (level == 29) {
		cout << "Lucky you go up" << endl;
		return 44;
	}
	else if (level == 33) {
		cout << "Lucky you go up" << endl;
		return 50;
	}
	else if (level == 49) {
		cout << "Unlucky you go down" << endl;
		return 2;
	}
	else if (level == 25) {
		cout << "Unlucky you go down" << endl;
		return 10;
	}
	else if (level == 34) {
		cout << "Unlucky you go down" << endl;
		return 23;
	}
	else if (level > 50) {
		return level - 50;
		cout << "Oops your exceed to 50, you go back to" << level - 50 << endl;
	}
	//Money
	else if (level == 12) {
		cout << "You Earn 5 Pesos" << endl;
		money1 += 5;
		return 12;
	}
	else if (level == 16) {
		cout << "You Earn 5 Pesos" << endl;
		money1 += 5;
		return 16;
	}
	//Rat
	if (level == 27) {
		if (money1 >= 5) {
			cout << "You Got a Rat" << endl;
			r1 += 1;
			money1 -= 5;
		}
		return 27;
	}
	else if (level == 38) {
		if (money1 >= 5) {
			cout << "You Got a Rat" << endl;
			r1 += 1;
			money1 -= 5;
		}
		return 38;
	}
	//Jeep
	if (level == 20) {
		if (money1 >= 5) {
			cout << "You Ride a jeep" << endl;
			money1 -= 5;
			return 50;
		}
		else {
			return 20;
		}

	}
	else if (level == 15) {
		if (money1 >= 5) {
			cout << "You Ride a jeep" << endl;
			money1 -= 5;
			return 30;
		}
		else {
			return 20;
		}
	}
	else {
		return level;
	}
	
}


int check2(int level) { // method to check for snake and ladders
	if (level == 10) {
		cout << "Lucky you go up" << endl;
		return 13;
	}
	else if (level == 19) {
		cout << "Lucky you go up" << endl;
		return 25;
	}
	else if (level == 23) {
		cout << "Lucky you go up" << endl;
		return 35;
	}
	else if (level == 29) {
		cout << "Lucky you go up" << endl;
		return 44;
	}
	else if (level == 33) {
		cout << "Lucky you go up" << endl;
		return 50;
	}
	else if (level == 49) {
		cout << "Unlucky you go down" << endl;
		return 2;
	}
	else if (level == 25) {
		cout << "Unlucky you go down" << endl;
		return 10;
	}
	else if (level == 34) {
		cout << "Unlucky you go down" << endl;
		return 23;
	}
	else if (level > 50) {
		return level - 50;
		cout << "Oops your exceed to 50, you go back to" << level - 50 << endl;
	}
		//Money
		else if (level == 12) {
			cout << "You Earn 5 Pesos" << endl;
			money2 += 5;
			return 12;
		}
		else if (level == 16) {
			cout << "You Earn 5 Pesos" << endl;
			money2 += 5;
			return 16;
		}
		//Rat
		if (level == 27) {
			if (money2 >= 5) {
				cout << "You Got a Rat" << endl;
				r2 += 1;
				money2 -= 5;
			}
			return 27;
		}
		else if (level == 38) {
			if (money2 >= 5) {
				cout << "You Got a Rat" << endl;
				r2 += 1;
				money2 -= 5;
			}
			return 38;
		}
		//Jeep
		if (level == 20) {
			if (money2 >= 5) {
				cout << "You ride a jeep" << endl;
				money2 -= 5;
				return 50;
			}
			else {
				return 20;
			}

		}
		else if (level == 15) {
			if (money2 >= 5) {
				cout << "You ride a jeep" << endl;
				money2 -= 5;
				return 30;
			}
			else {
				return 20;
			}
		}
	else {
		return level;
	}
	
}

int checkMoney2(int level) {
	if (level == 12) {
		cout << "You Earn 5 Pesos" << endl;
		r2 += 5;
		return 12;
	}
	else if (level == 16) {
		cout << "You Earn 5 Pesos" << endl;
		r2 += 5;
		return 12;
	}
};

int main()
{


	
	cout << " Enter name of Player 1: ";
	cin >> name1;
	cout << " Enter name of Player 2: ";
	cin >> name2;
	drawBoard(name1,name2,0,0,money1,money2, r1, r2);

	cout << "--------START----------" <<endl << endl;
	int t = 0;
	int op = 0;
	while (true) {
		cout << "****"<< name1 <<"'s Turn ****" <<endl;
		cout << " " << name1 << ": [1]Roll Dice [2] Quit: ";
		cin >> op;
		if (op == 1) {
			output = turn(0);
			cout << " Result of your roll: " << output << endl;
			p1Score += output;
			p1Score = check1(p1Score);
			//cout << " "<<name1 << "'s current level " << p1Score << endl << endl;
			
		}
		else if (op == 2) {
			cout << " " << name1 << " Quit The Game" << endl;
			cout << " " << name2 << "Wins";
			break;
		}
		cout << endl;
		drawBoard(name1,name2, p1Score, p2Score, money1, money2, r1, r2);

		cout << "****" << name2 << "'s Turn ****" << endl;
		cout << " " << name2 << ": [1]Roll Dice [2] Quit: ";
		cin >> op;
		if (op == 1) {
			output = turn(1);
			cout << " Result of your roll: " << output << endl;
			p2Score += output;
			p2Score = check2(p2Score);
			//cout << " " << name2 << " 's current level " << p2Score << endl << endl;
		}
		else if (op == 2) {
			cout << " " << name2 << " Quit The Game" <<endl;
			cout << " " << name1 << " Wins";
			break;
		}					
		cout << endl;
		drawBoard(name1, name2, p1Score, p2Score, money1, money2, r1, r2);

		if (p1Score == 50) {
			cout << " Player 1 Wins: " << name1;
			drawBoard(name1, name2, p1Score, p2Score, money1, money2, r1, r2);
			break;
		}
		else if (p2Score == 50) {
			cout << " Player 2 Wins: " << name2 << endl << endl;
			drawBoard(name1, name2, p1Score, p2Score, money1, money2, r1, r2);
			break;
		}
		else {
			cout << name1 << " 's current level " << p1Score <<endl;
			cout << name2 << " 's current level " << p2Score << endl << endl;
		}
		drawBoard(name1, name2, p1Score, p2Score, money1, money2, r1, r2);

	}
	cin.ignore();
	cin.get();
	return 0;
}


